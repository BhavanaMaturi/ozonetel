 $(document).ready(function() {
	 
	$(".owl-brand").owlCarousel(

         {
			
              items: 5,

              autoplay: true, //Set AutoPlay to 3 seconds

              autoplaySpeed: 2000,

             dotSpeed: 2000,

              nav: false,

			  mouseDrag: true,

              loop: true,

             dots:true,

             autoplayHoverPause: true,
			 
			  margin :20,
			 

          });
  });

  $(document).ready(function() {
	 
	$(".owl-testimonial").owlCarousel(

         {
			
              items: 1,

              autoplay: true, //Set AutoPlay to 3 seconds

              autoplaySpeed: 2000,

             dotSpeed: 2000,

              nav: false,

			  mouseDrag: true,

              loop: true,

             dots:true,

             autoplayHoverPause: true,
			 
			  margin :20
			 

          });
  });
    $(document).ready(function() {
	 
	$(".owl-leaders").owlCarousel(

         {
			
              items: 1,

              autoplay: false, //Set AutoPlay to 3 seconds

              autoplaySpeed: false,

             dotSpeed: 2000,

              nav: true,

			  mouseDrag:false,

              loop: true,

             dots:false,         
			 
			  margin :20,
			  
			  animateOut: 'fadeOut',
			  
			  navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
			   responsive:{
			0:{
				items:1,
				nav:false,
				autoplay: true ,
				 autoplaySpeed: 3000, //Set AutoPlay to 3 seconds
				  dots:true 
			},
			600:{
				items:1,
				nav:false
			},
			768:{
				items:1,
				nav:true
			},
			1000:{
				items:1, 
				nav:true,
				loop:false
			}
  		  }

          });
  });
 $(window).load(function () {
    $(".trigger_popup_fricc").click(function(){
       $('.hover_bkgr_fricc').show();
    });
    $('.hover_bkgr_fricc').click(function(){
        $('.hover_bkgr_fricc').hide();
    });
    $('.popupCloseButton').click(function(){
        $('.hover_bkgr_fricc').hide();
    });
});
                        