<?php
require_once("config.php");
require_once("functions.php");

db_connect();

$alias = trim(mysql_real_escape_string($_GET['alias']));

if (!preg_match("/^[a-zA-Z0-9_-]+$/", $alias)) {
  header("Location: ".SITE_URL, true, 301);
  exit();
}

if (($url = get_url($alias))) {
	$ret=insert_stat($_SERVER['HTTP_REFERER'],$alias,$_SERVER['REMOTE_ADDR'],$_SERVER['HTTP_USER_AGENT']);
	//var_dump($url);
	//var_dump($alias);
    header("Location: $url", true, 302);
    exit();
}

header("Location: ".SITE_URL, true, 302);
