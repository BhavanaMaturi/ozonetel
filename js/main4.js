(function () {
    var bodyEl = document.body,
        content = document.querySelector('.content-wrap'),
        openbtn = document.getElementById('open-button'),
        closebtn = document.getElementById('close-button'),
        closebtn1 = document.getElementById('close-button1'),
        closebtn2 = document.getElementById('close-button2'),
        closebtn3 = document.getElementById('close-button3'),
        closebtn4 = document.getElementById('close-button4'),
        isOpen = false,
        morphEl = document.getElementById('morph-shape'),
        s = Snap(morphEl.querySelector('svg'));
    path = s.select('path');
    initialPath = this.path.attr('d'), steps = morphEl.getAttribute('data-morph-open').split(';');
    stepsTotal = steps.length;
    isAnimating = false;

    function init() {
        initEvents();
    }

    function initEvents() {
        openbtn.addEventListener('click', toggleMenu);
        if (closebtn) {
            closebtn.addEventListener('click', toggleMenu);
        }
        if (closebtn1) {
            closebtn1.addEventListener('click', toggleMenu);
        }
        if (closebtn2) {
            closebtn2.addEventListener('click', toggleMenu);
        }
        if (closebtn3) {
            closebtn3.addEventListener('click', toggleMenu);
        }
        if (closebtn4) {
            closebtn4.addEventListener('click', toggleMenu);
        }
        content.addEventListener('click', function (ev) {
            var target = ev.target;
            if (isOpen && target !== openbtn) {
                toggleMenu();
            }
        });
    }

    function toggleMenu() {
        if (isAnimating) return false;
        isAnimating = true;
        if (isOpen) {
            classie.remove(bodyEl, 'show-menu');
            setTimeout(function () {
                path.attr('d', initialPath);
                isAnimating = false;
            }, 300);
        } else {
            classie.add(bodyEl, 'show-menu');
            var pos = 0,
                nextStep = function (pos) {
                    if (pos > stepsTotal - 1) {
                        isAnimating = false;
                        return;
                    }
                    path.animate({
                        'path': steps[pos]
                    }, pos === 0 ? 400 : 500, pos === 0 ? mina.easein : mina.elastic, function () {
                        nextStep(pos);
                    });
                    pos++;
                };
            nextStep(pos);
        }
        isOpen = !isOpen;
    }
    init();
})();
